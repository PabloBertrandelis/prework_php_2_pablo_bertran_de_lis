<?php
    function compruebaVocales($objeto) {
        $objeto = strtolower($objeto);
        $palabraDescompuesta = str_split($objeto,1);

        /*Comprobar si hay alguna vocal (a, e, i, o, u)
        Por cada una que aparezca al menos una vez:
        ++ a $contador*/
        $contador = 0;
        if (in_array('a', $palabraDescompuesta)) {
            $contador++;
        }
        if (in_array('e', $palabraDescompuesta)) {
            $contador++;
        }
        if (in_array('i', $palabraDescompuesta)) {
            $contador++;
        }
        if (in_array('o', $palabraDescompuesta)) {
            $contador++;
        }
        if (in_array('u', $palabraDescompuesta)) {
            $contador++;
        }

        /*Respuesta true si están las 5
        o false si no lo están*/
        $salida = ($contador == 5) ? true : false;
        return $salida;
    }
    
    //No funciona con acentos
    $palabra = "prueba cn varias palabras";
    $valor = compruebaVocales($palabra);
    if ($valor){
        echo "LA PALABRA CONTIENE LAS 5 VOCALES\n";
    } else {
        echo "LA PALABRA NO CONTIENE LAS 5 VOCALES\n";
    }
    echo "La palabra/frase era: \"$palabra\".\n";
?>